new Vue({
    el: '#app',
    data: {
        player: 100,
        monster: 100,
        hasGameStarted: false,
        turns: []
    },
    methods: {
        start: function() {
            this.hasGameStarted = true;
            this.player = 100;
            this.monster = 100;
            this.turns = [];
        },
        calculateDamage: function(min, max) {
            return Math.max(Math.floor(Math.random() * max) + 1, min);
        },
        monsterAttack: function() {
            var damage = this.calculateDamage(5, 12);
            this.player -= damage;
            this.checkWin();
            this.turns.push({
                isPlayer: false,
                text: 'Monster hits Player for ' + damage
            });
        },
        attack: function () {
            var damage = this.calculateDamage(3, 10);
            this.monster -= damage;
            this.turns.push({
                isPlayer: true,
                text: 'Player hits Monster for ' + damage
            });
            if (this.checkWin()) {
                return;
            }
            this.monsterAttack();
        },
        specialAttack: function () {
            var damage = this.calculateDamage(10, 20);
            this.monster -= damage;
            this.turns.push({
                isPlayer: true,
                text: 'Player hits Monster hard for ' + damage
            });
            if (this.checkWin()) {
                return;
            }
            this.monsterAttack();
        },
        heal: function () {
            if (this.player <= 90) {
                this.player += 10;
            } else {
                this.player = 100;
            }
            this.turns.push({
                isPlayer: true,
                text: 'Player heals for 10'
            });
            this.monsterAttack();
        },
        leave: function () {
            this.hasGameStarted = false;
        },
        checkWin: function() {
            if (this.monster <= 0) {
                if (confirm('Winner Winner! Start New Game?')) {
                    this.start();
                } else {
                    this.hasGameStarted = false;
                }
                return true;
            } else if (this.player <= 0) {
                if (confirm('Defeat! Start New Game?')) {
                    this.start();
                } else {
                    this.hasGameStarted = false;
                }
                return true;
            }
            return false;
        }
    }
})